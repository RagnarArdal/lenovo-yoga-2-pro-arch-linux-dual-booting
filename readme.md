Lenovo Yoga 2 Pro Arch Linux Dual-Booting
=========================================

About
-----

This is a step-by-step guide to [dual-booting](https://en.wikipedia.org/wiki/Multi-booting) [Arch Linux](https://www.archlinux.org/) alongside the factory OS of the [Lenovo Yoga 2 Pro](http://shop.lenovo.com/us/en/laptops/lenovo/yoga-laptop-series/yoga-laptop-2-pro/); i.e., Windows 8. It also assumes you want a shared NTFS partition that both operating systems can access. In addition, this guide goes through creating a recovery drive so you can safely utilize the space taken up by the recovery partition. I was inclined to do so because my recovery partition was something like 40 GBs.

The guide mostly relies on Windows 8 documentation, the [rEFInd](http://www.rodsbooks.com/refind/) documentation, and [ArchWiki](https://wiki.archlinux.org/) articles such as the [Arch Linux Beginner's Guide](https://wiki.archlinux.org/index.php/Beginners'_guide).

I have only tested this with my personal Lenovo Yoga 2 Pro, but one can probably use this as a reference when doing the same with other similar laptops. Mine was not fresh out of the factory, so my BIOS settings may have been different. Please, [contact me](mailto:RagnarArdal@gmail.com) if you encounter a bump in the road so I can rectify this guide.

Materials
---------

In addition to a rad laptop and internet connection, you will need two USB drives:

+ one (maybe 16 GB, but size needed will be determined in a later step) USB drive for use as a permanent dedicated recovery drive, and
+ one (maybe 2 GB) USB drive for the temporarily needed [Arch Linux LiveUSB](https://wiki.archlinux.org/index.php/USB_flash_installation_media).

Instructions
------------

0. Obtain a standard working Windows 8 (factory) setup on your laptop. If you have followed these instructions before, you can use your recovery drive to do so. The usage of the recovery drive is briefly described in the Appendix of this document.
	+ I have already upgraded to Windows 10.
	> Don't fret, you have options if you want to downgrade, and some of them are detailed in the Appendix of this document.
	+ I have already upgraded to Windows 10 and want to dual-boot Arch Linux alongside it.
	> All of the steps below have a corresponding (if not equivalent) one in Windows 10 so you should be able to follow this guide just fine. I'd still recommend making a Windows 8 recovery drive (as described in the next step), rather than a Windows 10 one, so you can downgrade to Windows 8 at any point in the future.
	+ Can I upgrade my Windows 8 to Windows 10 after establishing the Arch Linux dual-boot?
	> No, the Windows 10 upgrade will mess up the partitions. There may be a way to circumvent this, but I have no knowledge of it.
0. [Create a recovery drive for Windows 8](http://windows.microsoft.com/en-us/windows-8/create-usb-recovery-drive). Skip this step if you already have one for your machine, or do not want one (not recommended, as we will be deleting the recovery partition).
	0. Press the Windows key (or `Ctrl + Esc`) to open the Start screen.
	0. Type "recovery drive" and select "Create a recovery drive" to run the Recovery Media Creator.
	0. In the Recovery Drive window, press Next. __IMPORTANT!__ Make sure the "Copy the recovery partition from the PC to the recovery drive" check box is selected before you do so.
	0. Now the Recovery Media Creator will tell you how big your USB drive has to be.
	0. Grab an appropriate USB drive and use it to finalize the recovery drive creation. You will be offered the option of deleting the partition, but that is unnecessary.
0. Shrink your OS partition.
	0. Open up the Disk Management tool by pressing `Win + x`, `k`. It should report something similar to this: ![A picture of the Disk Management tool. The SSD has a total of six partitions. The biggest partition is the one containing the OS, but the last two partitions contain Lenovo drivers/bloatware and recovery files, respectively, and amount to 40 GBs of wasted space!](https://bytebucket.org/RagnarArdal/lenovo-yoga-2-pro-arch-linux-dual-booting/raw/7f9b94f5d38421bf6c897149f23faa064dbba730/images/windows_partition/before.png "Holy cow! How come there are 40 GBs over there I can hardly use!?")
	0. Right click the __C:__ drive---the one containing the operating system---and select "Shrink Volume...".
	0. Now, put a number in the "Enter the amount of space to shrink in MB" field, and press the Shrink button. I decided to halve my Windows partition, and you can see the result in this image: ![A picture of the Disk Management tool. The SSD has a total of seven partitions; one more than last time. The __C:__ drive is half as big, and there is a just about equally big unallocated partition following it.](https://bytebucket.org/RagnarArdal/lenovo-yoga-2-pro-arch-linux-dual-booting/raw/f04f05f851fecae6bd4cf20b8195f8f9dfd15b34/images/windows_partition/resized.png "We didn't need that much space on our OS partition anyway...")
0. [Install the rEFInd Boot Manager](http://www.rodsbooks.com/refind/installing.html#windows). In the Appendix section of this document you will find some __tree__ outputs that may (or may not) help you understand what exactly happens when you install the rEFInd Boot Manager.
	0. Download and extract the [binary zip file](http://www.rodsbooks.com/refind/getting.html) to your __C:__ drive (or anywhere, really, but I'll be assuming the __C:__ drive). If you need a program for extracting, I can recommend [7-Zip](http://www.7-zip.org/).
	0. Open a command prompt with administrative privileges. (Shortcut: `Win + x`, `a`.)
	0. Type the command __powercfg /h off__ to disable the fast shutdown feature of Windows 8. (I'm not sure if this step is needed, but it's in the source guide.)
	0. Type __mountvol S: /S__ to make the ESP---i.e., the [EFI System Partition](https://en.wikipedia.org/wiki/EFI_System_partition)---available as the drive __S:__ from this command prompt.
	0. Change into the main rEFInd package directory, so that the refind subdirectory is visible when you type __dir__. Just type __cd\\__, followed by __dir__ to check for the refind subdirectory. If the refind directory isn't there, just __cd__ to a likely suspect, e.g. __cd refind-bin-0.9.0__, and look again with __dir__ until you find it.
	0. Type __xcopy /E refind S:\EFI\refind\\__ to copy the refind directory tree to the ESP's EFI directory.
	0. Type __S:__ to change to the ESP.
	0. Type __cd EFI\refind__ to change into the refind subdirectory.
	0. Type __rd /S drivers_ia32 tools_ia32__ and __del refind_ia32.efi__ to remove files/drivers you do not need. (This you can look further into, but I've decided not to.)
	0. Type __rename refind.conf-sample refind.conf__ to rename rEFInd's configuration file.
	0. Type __bcdedit /set {bootmgr} path \EFI\refind\refind_x64.efi__ to set rEFInd as the default EFI boot program.
0. [Create Arch Linux LiveUSB using Rufus](https://wiki.archlinux.org/index.php/USB_flash_installation_media#Using_Rufus).
	0. Download both [Rufus](https://rufus.akeo.ie/) and the [Arch Linux image](https://www.archlinux.org/download/).
	0. Plug the USB to be used as a LiveUSB into your machine and start Rufus.
	0. In Rufus, select the USB drive and set "Partition scheme and target system type" to "GPT partition scheme for UEFI".,
	0. Make sure "Create a bootable disk using" is checked and "ISO Image" is selected.
	0. Select the Arch Linux image as the image to be used.
	0. Press Start and wait for Rufus to finish.
		+ You may be prompted to download a new version of syslinux. If you are, let Rufus download it.
0. Boot the Arch Linux LiveUSB. Make sure the Arch Linux LiveUSB is plugged into the machine and reboot/turn it on. You should be greeted by the rEFInd Boot Manager. Use the arrow and Enter keys to select the Arch Linux bootloader. Next select "Arch Linux archiso x86_64 UEFI USB" or just wait for it to boot on its own.
> (Optional) The 3200x1800 resolution of the Lenovo Yoga 2 Pro results in terribly small font. As a short term fix during the installation process, you may want to change fonts; e.g., by typing __setfont iso01-12x22__.
0. Perform [pre-installation](https://wiki.archlinux.org/index.php/Installation_guide#Pre-installation).
	0. [Prepare the storage media](https://wiki.archlinux.org/index.php/Beginners'_guide#Prepare_the_storage_devices).
		0. Use __lsblk__ to determine the name of the drive---its size should be a dead giveaway. For me, this was "sda" and we'll be using that in the examples to come.
		0. Run cgdisk on the device whose name you just determined; e.g., __cgdisk /dev/sda__.
		0. Delete the partitions after the Windows partition.
		0. Create a new shared data partition; i.e., move to the "Free space" partition, select New, use the default first sector, select a size (I chose 100G), and set the type as 0700 for Microsoft basic data.
		0. Create a new Linux partition; i.e., move to the "Free space" partition, select New, and press the Enter key a bunch of times.
		0. Optionally, you can name the partitions. Here is a picture of cgdisk on my machine before and after the partitioning: TODO
		0. Write the partition table to the drive by selecting Write, and exit cgdisk by selecting Quit.
	0. [Format the new partitions with appropriate file systems](https://wiki.archlinux.org/index.php/Beginners'_guide#File_systems_and_swap).
		+ As an example, the commands I used were __mkfs.ntfs /dev/sda6__ and __mkfs.btrfs /dev/sda7__, although __mkfs.ext4 /dev/sda7__ in lieu of the latter is probably more common.
	0. [Mount the root partition](https://wiki.archlinux.org/index.php/Installation_guide#Mount_the_partitions). I would type __mount /dev/sda7 /mnt__ to do so, because sda7 is where I want my Arch Linux to reside.
	0. [Establish an internet connection](https://wiki.archlinux.org/index.php/Beginners'_guide#Establish_an_internet_connection) by typing __rfkill unblock wlan__ to [unblock the wireless card](https://wiki.archlinux.org/index.php/Lenovo_Ideapad_Yoga_2_Pro#The_ideapad_laptop_module) and using the __wifi-menu__ tool to connect to a wireless network.
0. [Install Arch Linux](https://wiki.archlinux.org/index.php/Installation_guide#Installation).
	0. Select the mirrors either [manually](https://wiki.archlinux.org/index.php/Mirrors#Enabling_a_specific_mirror) by editing __/etc/pacman.d/mirrorlist__ or [automatically](https://wiki.archlinux.org/index.php/Mirrors#List_by_speed).
	0. Type __pacstrap /mnt__ to have the pacstrap script install the base group.
	0. [Configure the system](https://wiki.archlinux.org/index.php/Installation_guide#Configure_the_system). I won't go into much detail regarding this so I suggest following the hyperlink.
		0. Generate a fstab file with __genfstab -p /mnt >> /mnt/etc/fstab__.
		0. Change root into the new system with __arch-chroot /mnt__.
		0. Set the hostname with __echo computer_name > /etc/hostname__, replacing "computer_name" with a hostname of your choice.
		0. Set the time zone. For me, this involved typing __ln -sf /usr/share/zoneinfo/Iceland /etc/localtime__.
		0. Uncomment the needed locales in __/etc/locale.gen__, then generate them with __locale-gen__.
		0. Set the locale with __echo LANG=your_locale > /etc/locale.conf__ where "your_locale" is one of the ones listed by __locale -a__.
		0. Create a new initial RAM disk with __mkinitcpio -p linux__.
		0. Create a root password with __passwd__.
		0. Install dialog and wpa_supplicant by typing __pacman -S dialog wpa_supplicant__ so you can run wifi-menu on your system.
		0. Type __exit__ to exit the chroot environment.
	0. Turn off the machine by typing __shutdown -h now__, remove the LiveUSB. __NOTE!__ You no longer need the LiveUSB so you can reformat it.
0. Sometimes Windows won't notice that the partitioning of the drive has changed. If so, re-scan the partitions in Windows so it can see the new partitions.
	0. Boot into Windows 8.
	0. Press `Win + R` to open the Run window.
	0. In the Run window, type __Diskpart__ and press the Enter key.
	0. In the Diskpart tool, type __rescan__ and press the Enter key.
	0. Close the Diskpart tool.

Appendix
--------

### Using the recovery drive

0. Plug your Windows 8 recovery USB drive into a free USB slot on your machine.
0. Completely power off your machine and then turn it on using the Novo button (the small circular button next to the power button).
0. Go to the Boot Menu and select your recovery drive.
0. After the drive has booted, select a language and keyboard layout, navigate to `Troubleshoot > Reset your PC` and choose a target operating system (Windows 8.1 in my case).
0. Follow the instructions to set up the operating system.

### Downgrading from Windows 10

+ Windows 10 offers a downgrade which, in short, consists of navigating to `Start > Settings > Update & security > Recovery` and pressing the "Get started" in the "Go back to Windows _" section, where the underscore is your old Windows version. If this option isn't available, then Windows 10 has already cleaned up the files necessary to downgrade, and you have to find another way to do so.
+ Using your Windows 8 recovery drive, you can downgrade by doing a clean install. However, the Windows 10 installation gets in the way, so we have to do a little trick to make it disappear. The following steps outline the process:
	0. Plug your Windows 8 recovery USB drive into a free USB slot on your machine.
	0. Completely power off your machine and then turn it on using the Novo button (the small circular button next to the power button).
	0. Go to the Boot Menu and select your recovery drive.
	0. After the drive has booted, select a language and keyboard layout. (Remember to use your glorious touchscreen capabilities to navigate; the mouse sensitivity is atrocious.)
	0. Navigate to `Troubleshoot > Advanced Options > Command Prompt`.
	0. Now we will basically delete the contents of your SSD. If you want to know more, read up on some [diskpart documentation](https://technet.microsoft.com/en-us/library/cc766465(v=ws.10).aspx). In the command prompt that opens up, enter the following commands:
		0. __diskpart__
		0. __list disk__
		0. __select disk system__
		0. __clean__
		0. __exit__
	0. Close the command prompt and navigate to `Troubleshoot > Reset your PC` and choose a target operating system (Windows 8.1 in my case).
	0. Follow the on-screen instructions to set up the operating system.
+ There are still more options to downgrade that simply won't covered by this guide. Use your own tech savvy or Google to make it happen.

### ESP Partition
This section contains, among other things, output acquired by running __tree__ and __tree /F__ in a command prompt in the ESP partition.

ESP structure before installing the rEFInd Boot Manager:

```
S:.
└───EFI
    ├───Microsoft
    │   └───Boot
    │       ├───bg-BG
    │       ├───cs-CZ
    │       ├───da-DK
    │       ├───de-DE
    │       ├───el-GR
    │       ├───en-GB
    │       ├───en-US
    │       ├───es-ES
    │       ├───et-EE
    │       ├───fi-FI
    │       ├───fr-FR
    │       ├───hr-HR
    │       ├───hu-HU
    │       ├───it-IT
    │       ├───ja-JP
    │       ├───ko-KR
    │       ├───lt-LT
    │       ├───lv-LV
    │       ├───nb-NO
    │       ├───nl-NL
    │       ├───pl-PL
    │       ├───pt-BR
    │       ├───pt-PT
    │       ├───qps-ploc
    │       ├───ro-RO
    │       ├───ru-RU
    │       ├───sk-SK
    │       ├───sl-SI
    │       ├───sr-Latn-CS
    │       ├───sr-Latn-RS
    │       ├───sv-SE
    │       ├───tr-TR
    │       ├───uk-UA
    │       ├───zh-CN
    │       ├───zh-HK
    │       ├───zh-TW
    │       ├───Fonts
    │       └───Resources
    │           └───en-US
    └───Boot
```

```
S:.
└───EFI
    ├───Microsoft
    │   └───Boot
    │       │   BCD
    │       │   boot.stl
    │       │   bootmgfw.efi
    │       │   bootmgr.efi
    │       │   memtest.efi
    │       │
    │       ├───bg-BG
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───cs-CZ
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───da-DK
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───de-DE
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───el-GR
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───en-GB
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───en-US
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───es-ES
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───et-EE
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───fi-FI
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───fr-FR
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───hr-HR
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───hu-HU
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───it-IT
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───ja-JP
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───ko-KR
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───lt-LT
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───lv-LV
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───nb-NO
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───nl-NL
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───pl-PL
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───pt-BR
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───pt-PT
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───qps-ploc
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───ro-RO
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───ru-RU
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───sk-SK
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───sl-SI
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───sr-Latn-CS
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───sr-Latn-RS
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───sv-SE
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───tr-TR
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───uk-UA
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───zh-CN
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───zh-HK
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───zh-TW
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───Fonts
    │       │       chs_boot.ttf
    │       │       cht_boot.ttf
    │       │       jpn_boot.ttf
    │       │       kor_boot.ttf
    │       │       malgunn_boot.ttf
    │       │       malgun_boot.ttf
    │       │       meiryon_boot.ttf
    │       │       meiryo_boot.ttf
    │       │       msjhn_boot.ttf
    │       │       msjh_boot.ttf
    │       │       msyhn_boot.ttf
    │       │       msyh_boot.ttf
    │       │       segmono_boot.ttf
    │       │       segoen_slboot.ttf
    │       │       segoe_slboot.ttf
    │       │       wgl4_boot.ttf
    │       │
    │       └───Resources
    │           │   bootres.dll
    │           │
    │           └───en-US
    │                   bootres.dll.mui
    │
    └───Boot
            bootx64.efi
```

ESP structure after installing the rEFInd Boot Manager (the only addition is the EFI/refind directory):

```
S:.
└───EFI
    ├───Microsoft
    │   └───Boot
    │       ├───bg-BG
    │       ├───cs-CZ
    │       ├───da-DK
    │       ├───de-DE
    │       ├───el-GR
    │       ├───en-GB
    │       ├───en-US
    │       ├───es-ES
    │       ├───et-EE
    │       ├───fi-FI
    │       ├───fr-FR
    │       ├───hr-HR
    │       ├───hu-HU
    │       ├───it-IT
    │       ├───ja-JP
    │       ├───ko-KR
    │       ├───lt-LT
    │       ├───lv-LV
    │       ├───nb-NO
    │       ├───nl-NL
    │       ├───pl-PL
    │       ├───pt-BR
    │       ├───pt-PT
    │       ├───qps-ploc
    │       ├───ro-RO
    │       ├───ru-RU
    │       ├───sk-SK
    │       ├───sl-SI
    │       ├───sr-Latn-CS
    │       ├───sr-Latn-RS
    │       ├───sv-SE
    │       ├───tr-TR
    │       ├───uk-UA
    │       ├───zh-CN
    │       ├───zh-HK
    │       ├───zh-TW
    │       ├───Fonts
    │       └───Resources
    │           └───en-US
    ├───Boot
    └───refind
        ├───drivers_x64
        ├───icons
        └───tools_x64
```

```
S:.
└───EFI
    ├───Microsoft
    │   └───Boot
    │       │   BCD
    │       │   boot.stl
    │       │   bootmgfw.efi
    │       │   bootmgr.efi
    │       │   memtest.efi
    │       │
    │       ├───bg-BG
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───cs-CZ
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───da-DK
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───de-DE
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───el-GR
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───en-GB
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───en-US
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───es-ES
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───et-EE
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───fi-FI
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───fr-FR
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───hr-HR
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───hu-HU
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───it-IT
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───ja-JP
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───ko-KR
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───lt-LT
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───lv-LV
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───nb-NO
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───nl-NL
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───pl-PL
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───pt-BR
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───pt-PT
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───qps-ploc
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───ro-RO
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───ru-RU
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───sk-SK
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───sl-SI
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───sr-Latn-CS
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───sr-Latn-RS
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───sv-SE
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───tr-TR
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───uk-UA
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │
    │       ├───zh-CN
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───zh-HK
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───zh-TW
    │       │       bootmgfw.efi.mui
    │       │       bootmgr.efi.mui
    │       │       memtest.efi.mui
    │       │
    │       ├───Fonts
    │       │       chs_boot.ttf
    │       │       cht_boot.ttf
    │       │       jpn_boot.ttf
    │       │       kor_boot.ttf
    │       │       malgunn_boot.ttf
    │       │       malgun_boot.ttf
    │       │       meiryon_boot.ttf
    │       │       meiryo_boot.ttf
    │       │       msjhn_boot.ttf
    │       │       msjh_boot.ttf
    │       │       msyhn_boot.ttf
    │       │       msyh_boot.ttf
    │       │       segmono_boot.ttf
    │       │       segoen_slboot.ttf
    │       │       segoe_slboot.ttf
    │       │       wgl4_boot.ttf
    │       │
    │       └───Resources
    │           │   bootres.dll
    │           │
    │           └───en-US
    │                   bootres.dll.mui
    │
    ├───Boot
    │       bootx64.efi
    │
    └───refind
        │   refind_x64.efi
        │   refind.conf
        │
        ├───drivers_x64
        │       btrfs_x64.efi
        │       ext2_x64.efi
        │       ext4_x64.efi
        │       hfs_x64.efi
        │       iso9660_x64.efi
        │       LICENSE.txt
        │       LICENSE_GPL.txt
        │       ntfs_x64.efi
        │       reiserfs_x64.efi
        │
        ├───icons
        │       arrow_left.png
        │       arrow_right.png
        │       boot_linux.png
        │       boot_win.png
        │       func_about.png
        │       func_exit.png
        │       func_firmware.png
        │       func_reset.png
        │       func_shutdown.png
        │       os_altlinux.png
        │       os_arch.png
        │       os_centos.png
        │       os_chrome.png
        │       os_clover.png
        │       os_debian.png
        │       os_ecomstation.png
        │       os_fatdog.png
        │       os_fedora.png
        │       os_freebsd.png
        │       os_freedos.png
        │       os_funtoo.png
        │       os_gentoo.png
        │       os_gummiboot.png
        │       os_haiku.png
        │       os_hwtest.png
        │       os_kali.png
        │       os_kubuntu.png
        │       os_legacy.png
        │       os_linux.png
        │       os_linuxmint.png
        │       os_lubuntu.png
        │       os_mac.png
        │       os_mageia.png
        │       os_mandriva.png
        │       os_manjaro.png
        │       os_mythbuntu.png
        │       os_netbsd.png
        │       os_network.png
        │       os_openbsd.png
        │       os_opensuse.png
        │       os_redhat.png
        │       os_refind.png
        │       os_refit.png
        │       os_slackware.png
        │       os_suse.png
        │       os_ubuntu.png
        │       os_unknown.png
        │       os_win.png
        │       os_win8.png
        │       os_xubuntu.png
        │       tool_apple_rescue.png
        │       tool_memtest.png
        │       tool_mok_tool.png
        │       tool_netboot.png
        │       tool_part.png
        │       tool_shell.png
        │       tool_windows_rescue.png
        │       transparent.png
        │       vol_external.png
        │       vol_internal.png
        │       vol_net.png
        │       vol_optical.png
        │
        └───tools_x64
                gptsync_x64.efi
```